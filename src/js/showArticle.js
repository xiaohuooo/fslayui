$(document).ready(() => { // 等待 DOM 加载完毕后执行
    const API_URL = 'http://121.196.17.169:8088/api/papers/search'; // 定义 API 地址
    const requestData = {};
    requestData['searchey'] = '0'
    requestData['page'] = 0
    requestData['pageSize'] = 3

    $.ajax({
        url: API_URL,
        type: 'POST',
        requestData,
        dataType: 'json',
        success: (resp) => { // 请求成功时执行
            // 将响应显示在页面上
            if (resp && resp.data.code === 200) {
                data = resp.data.data.papers[0]
                console.log(resp)
                // this.setAuthor(this.articleList)
                const titleElem = document.querySelector('#article-title');
                titleElem.textContent = data.title;

                const authorElem = document.querySelector('#article-author');
                authorElem.textContent = data.authors;

                // const contentElem = document.createElement('div');
                // contentElem.innerHTML = data.content;
            } else {
                console.log('连接出错')
            }
            // const resultContainer = $('#result-container');
            // resultContainer.text(JSON.stringify(response));
        },
        error: (xhr, status, error) => { // 请求失败时执行
            console.error(error); // 输出错误信息
        },
    });
});

layui.use(function () {
    var layer = layui.layer;
});
layui.use(function () {
    var dropdown = layui.dropdown;
    // 自定义内容
    dropdown.render({
        elem: '#ID-dropdown-demo-content',
        data: [{
            title: 'menu item 1',
            id: 100
        }, {
            title: 'menu item 2',
            id: 101
        }, {
            title: 'menu item 3',
            id: 102
        }],
        click: function (obj) {
            this.elem.find('a').href(obj.title);
        }
    });
});


const imagesArray = [
    { src: '../images/figure_1.png', name: 'Image 1' },
    { src: '../images/logo.png', name: 'Image 2' },
    { src: '../images/figure_1.png', name: 'Image 3' },
    { src: '../images/figure_1.png', name: 'Image 4' },
    { src: '../images/figure_1.png', name: 'Image 5' },
    { src: '../images/figure_1.png', name: 'Image 6' },
    { src: '../images/figure_1.png', name: 'Image 7' },
    { src: '../images/figure_1.png', name: 'Image 8' },
    { src: '../images/figure_1.png', name: 'Image 9' },
    { src: '../images/figure_1.png', name: 'Image 10' },
];

const container = document.querySelector('.image-container');
const row = container.querySelector('.row');

function createImageElement(image) {
    const bigDiv = document.createElement('div');
    bigDiv.classList.add('imageDiv')
    const imageDiv = document.createElement('div');
    imageDiv.classList.add('image');

    const imageImg = document.createElement('img');
    imageImg.setAttribute('src', image.src);
    imageDiv.appendChild(imageImg);

    bigDiv.appendChild(imageDiv);
    const imageName = document.createElement('div');
    imageName.classList.add('name');
    imageName.textContent = image.name;
    bigDiv.appendChild(imageName);

    return bigDiv;
}

function displayImages(images) {
    const numImages = images.length;
    const numEmpty = numImages % 4 === 0 ? 0 : 4 - numImages % 4;
    const emptyDivs = Array.from({ length: numEmpty }, () => document.createElement('div'));

    const imageElements = images
        .map(createImageElement)
        .concat(emptyDivs);

    row.innerHTML = '';
    imageElements.forEach(image => row.appendChild(image));
}

displayImages(imagesArray);

layui.use(function () {
    var laypage = layui.laypage;
    // 自定义文本
    laypage.render({
        elem: 'demo-laypage-text',
        count: 100,
        first: '首页',
        last: '尾页',
        prev: '<em>←</em>',
        next: '<em>→</em>'
    });
});

layui.use(function () {
    var laypage = layui.laypage;
    // 自定义文本
    laypage.render({
        elem: 'demo-laypage-text1',
        count: 100,
        first: '首页',
        last: '尾页',
        prev: '<em>←</em>',
        next: '<em>→</em>'
    });
});

//注意：选项卡 依赖 element 模块，否则无法进行功能性操作
layui.use('element', function () {
    var element = layui.element;

    element.on('tab(table_box)', function (data) {
        console.log(data);
        if (data.index == 0 || data.index == 1 || data.index == 2) {
            //这里的index表示有多少个选项卡
            $(document).resize()
        }
    });
});

var currPage = 1;
var pageCount;
function paged(res) {//res后台获取的json数据
    layui.use(['laypage', 'layer'], function () {
        var laypage = layui.laypage
        laypage.render({
            elem: 'pagesbox' // 分页容器id
            , count: res.count //总页数
            , limit: 10 //每页显示的数量
            , curr: currPage //当前页
            , layout: ['prev', 'page', 'count', 'next', 'skip'] //设置分页显示的内容
            , jump: function (obj, first) {
                currPage = obj.curr;  //这里是后台返回给前端的当前页数
                if (!first) { //点击跳页触发函数自身，并传递当前页：obj.curr  ajax 再次请求
                    getListData(currPage);
                }
            }
        });
    });
}
function getListData(currPage) {
    $.ajax({
        type: 'POST',
        url: "", // ajax请求路径
        data: {
            page: currPage, //当前页数
            limit: 10 //每页显示数量
        },
        dataType: 'json',
        success: function (res) {
            pageCount = res.count;
            document.getElementById("ul").innerHTML = res.data;//设置ul内容(因为我在后台将html格式的数据封装了，此处直接赋值给ul元素)
            paged(data);
        }
    });
};
$(function () {
    getListData(1);//获取数据
});

//获取后端数据
$(document).ready(function () {
    $.ajax({
        url: 'your-backend-url',
        type: 'GET',
        success: function (data) {
            $('#data').text(data);
        }
    });
});
