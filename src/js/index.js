$(document).ready(function () {
  $(".link-to-html").click(function () {
    window.location.href = "../views/pdf.html"; // 替换为目标HTML页面的路径
  });
});
function toggleDiv(class1, class2) {
  let div = $("." + class1);
  let button = $("." + class2);
  if (div.height() === 200) {
    div.animate(
      {
        height: div.get(0).scrollHeight,
      },
      "slow"
    );
    button.text("收起");
  } else {
    div.animate(
      {
        height: 200,
      },
      "slow"
    );
    button.text("展开全部");
  }
}
function loadMoreData(table) {
  var moreRow = table.find("tbody tr:last");
  var currentCount = table.find("tbody tr").length - 1; // 获取当前表格的所有tr数量
  console.error(currentCount);
  var page = Math.floor(table / 10) + 1; // 计算当前页码
  var pageSize = 10; // 每页显示的条数
  moreRow.remove();
  // 发起服务器请求
  // $.get("your_server_url", { page: page, pageSize: pageSize })
  //     .done(function(response) {
  //       var newData = response.data;
  var data = [
    {
      category: "化合物 & 药物",
      number: 266,
      conceptList: [
        { name: "glycosphingolipids", number: 0, ref: null },
        { name: "headache medicine", number: 0, ref: null },
        { name: "glycerophospholipid species", number: 0, ref: null },
        { name: "carbohydrate headgroup", number: 0, ref: null },
        { name: "pi(4,5)p2 headgroup", number: 0, ref: null },
        { name: "prophylactic headache medication", number: 0, ref: null },
        { name: "phosphatidylcholine headgroup", number: 0, ref: null },
      ],
    },
  ];
  var newData = data;
  // 将返回的数据添加到表格中
  $.each(data, function (index, item) {
    var conceptList = item.conceptList;
    $.each(conceptList, function (i, concept) {
      var newRow = $("<tr>");
      newRow.append($("<td>").html("<a href='#'>" + concept.name + "</a>"));
      newRow.append($("<td>").html("<a href='#'>" + concept.number + "</a>"));
      newRow.append(
        $("<td>").html("<a href='#'><img src='images/text.png'></a>")
      );
      table.find("tbody").append(newRow);
    });
  });
  table.find("tbody").append(moreRow);
  // currentCount += newData.length;

  // 判断是否还有更多数据，如果没有，则隐藏"more"链接
  // if (currentCount >= totalNumber) {
  //   moreLink.hide();
  // }
}

function loadMoreDataIn(table) {
  var moreRow = table.find("tbody tr:last");
  var currentCount = table.find("tbody tr").length - 1; // 获取当前表格的所有tr数量
  console.error(currentCount);
  var page = Math.floor(table / 10) + 1; // 计算当前页码
  var pageSize = 10; // 每页显示的条数
  moreRow.remove();
  // 发起服务器请求
  // $.get("your_server_url", { page: page, pageSize: pageSize })
  //     .done(function(response) {
  //       var newData = response.data;
  var data = [
    {
      targetConcept: {
        name: "glycosphingolipids",
        number: 2,
        ref: null,
      },
      middleConceptList: [
        { name: "chrohe", number: 1, ref: null },
        { name: "headache medicine", number: 1, ref: null },
        { name: "chronic headache", number: 1, ref: null },
      ],
    },
  ];
  // 将返回的数据添加到表格中
  $.each(data, function (index, item) {
    var targetConcept = item.targetConcept.name;
    var conceptList = item.middleConceptList;

    var tableRow = $("<tr>");
    var targetConceptCell = $("<td rowspan='" + conceptList.length + "'>").html(
      "<a href='#'>" + targetConcept + "</a>"
    );
    tableRow.append(targetConceptCell);
    var middleConceptCell = $("<td rowspan='" + conceptList.length + "'>").html(
      "<a href='#'><img src='images/text.png'></a>"
    );
    tableRow.append(middleConceptCell);
    $.each(conceptList, function (i, concept) {
      var middleConceptRow = $("<tr>");
      middleConceptRow.append(
        $("<td>").html("<a href='#'>" + concept.name + "</a>")
      );
      // tableRow.append($("<td>").html("<a href='#'>" + concept.number + "</a>"));
      middleConceptRow.append(
        $("<td>").html("<a href='#'><img src='images/text.png'></a>")
      );
      tableRow.append(middleConceptRow);
      table.find("tbody").append(tableRow);
      tableRow = $("<tr>"); // 添加空白行
    });
  });
  table.find("tbody").append(moreRow);
  // currentCount += newData.length;

  // 判断是否还有更多数据，如果没有，则隐藏"more"链接
  // if (currentCount >= totalNumber) {
  //   moreLink.hide();
  // }
}
function directSearchReponse(inputValue, data) {
  var tableContainer = $("#tableContainer");
  tableContainer.empty();
  var totalNumber = 0;
  for (var i = 0; i < data.length; i++) {
    var tableData = data[i];
    totalNumber += tableData.number;
    var tableWrapper = $("<div class='layui-col-xs12 layui-col-md2'>");
    var table = $("<table class='layui-table'>");
    var tableHead = $("<thead>");
    var tableBody = $("<tbody>");

    var tableTitleRow = $("<tr>");
    var tableTitleCell = $("<th colspan='3' style='text-align:center;'>").text(
      tableData.category + " (" + tableData.number + ")"
    );
    tableTitleRow.append(tableTitleCell);
    tableHead.append(tableTitleRow);

    var conceptList = tableData.conceptList;
    for (var j = 0; j < conceptList.length; j++) {
      var rowData = conceptList[j];

      var tableRow = $("<tr>");
      tableRow.append($("<td>").html("<a href='#'>" + rowData.name + "</a>"));
      tableRow.append($("<td>").html("<a href='#'>" + rowData.number + "</a>"));
      tableRow.append(
        $("<td>").html("<a href='#'><img src='images/text.png'></a>")
      );
      tableBody.append(tableRow);
    }
    var more = $("<tr>");
    var moreLink = $("<a>")
      .attr("href", "#")
      .text("more...")
      .addClass("more-link");

    tableContainer.on("click", "a.more-link", function () {
      var currentTable = $(this).closest("table"); // 获取当前点击的表格
      loadMoreData(currentTable);
    });
    // .fail(function() {
    //   console.error("请求失败");
    // });

    //   return false;
    // });

    more.append($("<td colspan='3'>").html(moreLink));
    tableBody.append(more);
    table.append(tableHead);
    table.append(tableBody);
    tableWrapper.append(table);
    tableContainer.append(tableWrapper);
  }
  $("#search_text_content").html(inputValue);
  $("#search_result_count").html(totalNumber);
}

function indrectSearchReponse(inputValue, data) {
  var tableContainer = $("#tableContainer");
  tableContainer.empty();
  var totalNumber = 0;
  var table = $("<table class='layui-table'>");
  var tableHead = $("<thead>");
  var tableBody = $("<tbody>");

  var tableTitleRow = $("<tr>");
  var tableTitleCell1 = $("<th colspan='2' style='text-align:center;'>").text(
    "目标概念"
  );
  var tableTitleCell2 = $("<th colspan='2' style='text-align:center;'>").text(
    "中间概念"
  );
  tableTitleRow.append(tableTitleCell1);
  tableTitleRow.append(tableTitleCell2);
  tableHead.append(tableTitleRow);
  var tableWrapper = $("<div class='layui-col-xs12 layui-col-md2'>");
  for (var i = 0; i < data.length; i++) {
    var tableData = data[i];
    totalNumber += tableData.targetConcept.number;
    var tableRow = $("<tr>");
    var targetConceptCell = $(
      "<td rowspan='" + tableData.middleConceptList.length + "'>"
    ).html("<a href='#'>" + tableData.targetConcept.name + "</a>");
    tableRow.append(targetConceptCell);
    var middleConceptCell = $(
      "<td rowspan='" + tableData.middleConceptList.length + "'>"
    ).html("<a href='#'><img src='images/text.png'></a>");
    tableRow.append(middleConceptCell);
    var middleConceptList = tableData.middleConceptList;
    for (var j = 0; j < middleConceptList.length; j++) {
      totalNumber += middleConceptList[j].number;
      var middleConceptRow = $("<tr>");
      var rowData = middleConceptList[j];
      middleConceptRow.append(
        $("<td>").html("<a href='#'>" + rowData.name + "</a>")
      );
      // tableRow.append($("<td>").html("<a href='#'>" + rowData.number + "</a>"));
      middleConceptRow.append(
        $("<td>").html("<a href='#'><img src='images/text.png'></a>")
      );
      tableRow.append(middleConceptRow);
      tableBody.append(tableRow);
      tableRow = $("<tr>"); // 添加空白行
    }
    var more = $("<tr>");
    var moreLink = $("<a>")
      .attr("href", "#")
      .text("more...")
      .addClass("more-link");

    tableContainer.on("click", "a.more-link", function () {
      var currentTable = $(this).closest("table"); // 获取当前点击的表格
      loadMoreDataIn(currentTable);
    });
    // // 添加空白行并设置高度为剩余空间
    // var remainingRowCount = Math.ceil(middleConceptList.length / 2);
    // var blankRow = $("<tr>").addClass("blank-row").css("height", remainingRowCount * 0);
    // tableBody.append(blankRow);
    more.append($("<td colspan='4'>").html(moreLink));
    tableBody.append(more);
    table.append(tableHead);
    table.append(tableBody);
    tableWrapper.append(table);
    tableContainer.append(tableWrapper);
  }

  $("#search_text_content").html(inputValue);
  $("#search_result_count").html(totalNumber);
}

function search(type) {
  var inputValue = $("#searchInput").val();
  if (inputValue === "") {
    layui.use("layer", function () {
      var layer = layui.layer;
      var inputOffset = $("#searchInput").offset();
      var left = inputOffset.left;
      var top = inputOffset.top + $("#searchInput").outerHeight();
      layer.msg("输入内容不能为空", {
        icon: 2,
        offset: [top + "px", left + "px"],
      });
    });
    return;
  }
  var data = [
    {
      category: "化合物 & 药物",
      number: 266,
      conceptList: [
        { name: "glycosphingolipids", number: 0, ref: null },
        { name: "headache medicine", number: 0, ref: null },
        { name: "glycerophospholipid species", number: 0, ref: null },
        { name: "carbohydrate headgroup", number: 0, ref: null },
        { name: "pi(4,5)p2 headgroup", number: 0, ref: null },
        { name: "prophylactic headache medication", number: 0, ref: null },
        { name: "phosphatidylcholine headgroup", number: 0, ref: null },
      ],
    },
    {
      category: "疾病及综合征",
      number: 102,
      conceptList: [
        { name: "disease1", number: 0, ref: null },
        { name: "disease2", number: 0, ref: null },
        { name: "disease3", number: 0, ref: null },
      ],
    },
  ];

  var data1 = [
    {
      targetConcept: {
        name: "glycosphingolipids",
        number: 2,
        ref: null,
      },
      middleConceptList: [
        { name: "chrohe", number: 1, ref: null },
        { name: "headache medicine", number: 1, ref: null },
        { name: "chronic headache", number: 1, ref: null },
      ],
    },
  ];

  if (type == "type1" || type == "type2" || type == "type3") {
    $("#suggest_area").hide();
    $("#search_result_area").show();
  }

  var selectedValues = [];
  $("input[name='AAA']:checked").each(function () {
    selectedValues.push($(this).attr("title"));
  });
  var requestData = {
    searchkey: inputValue,
    concepttype: selectedValues,
    page: 0,
    pageSize: 10,
  };

  console.log(requestData);

  if (type === "type1") {
    directSearchReponse(inputValue, data);
    $.post(
      "http://121.196.17.169:8088/medical/search",
      requestData,
      function (result) {
        if (result.code === 200) {
          //请求成功
          console.log(result);
          // $('#dataContainer').empty().append(table);
        } else {
          //请求失败
        }
      },
      "json"
    );
  } else if (type === "type2") {
    indrectSearchReponse(inputValue, data1);
    $.post(
      "http://121.196.17.169:8088/medical/search",
      requestData,
      function (result) {
        if (result.code === 200) {
          //请求成功
          console.log(result);
          // var table = createIndrectTable(data);
          // $('#dataContainer').empty().append(table);
        } else {
          //请求失败
        }
      },
      "json"
    );
  } else if (type === "type3") {
    window.location.href = "../article/showArticle.html";
    $.post(
      "http://121.196.17.169:8088/medical/search",
      requestData,
      function (result) {
        if (result.code === 200) {
          //请求成功
          console.log(result);
          // var table = createIndrectTable(data);
          // $('#dataContainer').empty().append(table);
        } else {
          //请求失败
        }
      },
      "json"
    );
  }
}

function navigateToProfilePage() {
  window.location.href = "../person/person.html";
}
