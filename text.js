// 后端返回数据说明,界面的UI和数据展示,是根据后端返回的数据做调整的

{
    "paperId": "论文id",
        "DOI": "DOI: 10.1109/tcomm.2020.3001125",  //DOI
            "paperTitle": "Intelligent Reflecting Surface: Practical Phase Shift Model and Beamforming Optimization", //标题
                "identifier": [     //认证信息
                    "CAS-2",
                    "JCR-Q1",
                    "SCIEEI"
                ],
                    "author": [        //作者,
                        "Samith Abeywickrama",
                        "Rui Zhang",
                        "Qingqing Wu",
                        "Chau Yuen"
                    ],
                        "publicTime": "IEEE Transactions on Communications Jun 2020",  //发布日期
                            "havepdf": "False",                //是否含有pdf
                                "versionCount": "5",				//共有多少个版本
                                    "versionCountinfo": [				//具体版本信息
                                        "2019-10-10 收录·arxiv.org",
                                        "2020-10-10 收录·arxiv.org",
                                        "2022-11-10 收录·xport.arxiv.org",
                                        "2023-11-10 收录·xport.arxiv.org",
                                        "2024-11-10 收录·xport.arxiv.org"
                                    ],									//摘要
                                        "abstract": "Reconfigurable intelligent surface (RIS) has recently emerged as a promising candidate to improve the energy and spectral efficiency of wireless communication systems. However, the unit modulus constraint on the phase shift of reflecting elements makes the design of optimal passive beamforming solution a challenging issue. The conventional approach is to find a suboptimal solution using the semi-definite relaxation (SDR) technique, yet the resultant suboptimal iterative algorithm usually incurs high complexity, hence is not amenable for real-time implementation. Motivated by this, we propose a deep learning approach for passive beamforming design in RIS-assisted systems. In particular, a customized deep neural network is trained offline using the unsupervised learning mechanism, which is able to make real-time prediction when deployed online. Simulation results show that the proposed approach maintains most of the performance while significantly reduces computation complexity when compared with SDR-based approach.",
                                            "knowledgeGraph": [   //知识图谱
                                                "https://www.bing.com/images/search?view=detailV2&ccid=YLGcjBrO&id=EB33EE85DFD445CD0A6685758E95439FFA1EC395&thid=OIP.YLGcjBrO5nTbTNOER6hRWQHaFB&mediaurl=https%3a%2f%2fth.bing.com%2fth%2fid%2fR.60b19c8c1acee674db4cd38447a85159%3frik%3dlcMe%252bp9DlY51hQ%26riu%3dhttp%253a%252f%252f5b0988e595225.cdn.sohucs.com%252fimages%252f20190806%252f386e74e31a5d4cb1a2ab49f38cb12958.JPG%26ehk%3da2s3FXKcNA%252ftXnEefQLcKvcLTD6eGQbfZBVHwcxux%252bw%253d%26risl%3d%26pid%3dImgRaw%26r%3d0&exph=576&expw=849&q=%e5%8c%bb%e5%ad%a6%e7%9f%a5%e8%af%86%e5%9b%be%e8%b0%b1%e7%9a%84%e5%9b%be%e7%89%87&simid=608043949466912099&FORM=IRPRST&ck=014C2BD5823443D5F31F00B5652DF57E&selectedIndex=0&ajaxhist=0&ajaxserp=0",
                                                "https://www.bing.com/images/search?view=detailV2&ccid=O2r5wjwb&id=D531196955D071ED93625176C9ACAA94A4293B2C&thid=OIP.O2r5wjwbCbpMCYJC6u_6qgHaGO&mediaurl=https%3a%2f%2fpic1.zhimg.com%2fv2-c635fda029c7cfdcaad7e23f5f5cd1dc_r.jpg&exph=907&expw=1080&q=%e5%8c%bb%e5%ad%a6%e7%9f%a5%e8%af%86%e5%9b%be%e8%b0%b1%e7%9a%84%e5%9b%be%e7%89%87&simid=608028139693149342&FORM=IRPRST&ck=FE65063DBCE4B4058C69203594734BF4&selectedIndex=0&idpp=overlayview&ajaxhist=0&ajaxserp=0",
                                                "https://www.bing.com/images/search?view=detailV2&ccid=2FiJdU70&id=5ECA5F2DF390B727AD997E3D5320435D9C94FC25&thid=OIP.2FiJdU70bBcdBH6Ey9TcSQHaG7&mediaurl=https%3a%2f%2fwww.pianshen.com%2fimages%2f628%2f89acf325f861e7ad0c74d03a09ed1e14.png&exph=808&expw=864&q=%e5%8c%bb%e5%ad%a6%e7%9f%a5%e8%af%86%e5%9b%be%e8%b0%b1%e7%9a%84%e5%9b%be%e7%89%87&simid=607993298926051658&FORM=IRPRST&ck=F36AD873A7680DD8B34D8CF1D365176D&selectedIndex=10&ajaxhist=0&ajaxserp=0",
                                                "https://www.bing.com/images/search?q=%E5%8C%BB%E5%AD%A6%E7%9F%A5%E8%AF%86%E5%9B%BE%E8%B0%B1%E7%9A%84%E5%9B%BE%E7%89%87&form=IQFRBA&id=4B06C6BB948A7CE2C0DB132378812D2C65134E0F&first=1&disoverlay=1"
                                            ],
                                                "chart_extraction": [    //图表提取
                                                    "https://www.bing.com/images/search?view=detailV2&ccid=HcqEUrTs&id=657884C5A07F60D9D12591828E639DE0CCE50D5C&thid=OIP.HcqEUrTsiFILguL39xZRkAHaEV&mediaurl=https%3a%2f%2fpic4.zhimg.com%2fv2-ab5e76c5c4dda3bab027596846cc2bbc_r.jpg&exph=518&expw=886&q=%e5%8c%bb%e5%ad%a6%e7%9f%a5%e8%af%86%e5%9b%be%e8%b0%b1%e7%9a%84%e5%9b%be%e7%89%87&simid=608028650791043986&FORM=IRPRST&ck=676DD2B3A398ED425204BC182E9606DF&selectedIndex=56&ajaxhist=0&ajaxserp=0",
                                                    "https://www.bing.com/images/search?view=detailV2&ccid=%2b0HknI5w&id=E943EC43D845A99B12505288154DAB2C2545FAAC&thid=OIP.-0HknI5wotioZrlXlJQuHQHaFO&mediaurl=https%3a%2f%2fpic4.zhimg.com%2fv2-1f169eb793efcd831c5a58af672b9b6b_r.jpg&exph=763&expw=1080&q=%e5%8c%bb%e5%ad%a6%e7%9f%a5%e8%af%86%e5%9b%be%e8%b0%b1%e7%9a%84%e5%9b%be%e7%89%87&simid=607994952453154619&FORM=IRPRST&ck=0183189BB659A78AC3C3B101BDC0A818&selectedIndex=30&ajaxhist=0&ajaxserp=0",
                                                    "https://www.bing.com/images/search?view=detailV2&ccid=CLR2epuT&id=71003CF8F2AC095A86FDA16DCEE3A3AEE68DCB6A&thid=OIP.CLR2epuTk9PwEeObIb28tQHaHF&mediaurl=https%3a%2f%2fwww.omaha.org.cn%2fdata%2fupload%2fueditor%2f20200903%2f5f508f620ee0c.png&exph=827&expw=864&q=%e5%8c%bb%e5%ad%a6%e7%9f%a5%e8%af%86%e5%9b%be%e8%b0%b1%e7%9a%84%e5%9b%be%e7%89%87&simid=607987814245350677&FORM=IRPRST&ck=201B6C2B542319C947A6FBE6E1F4C64E&selectedIndex=0&idpp=overlayview&ajaxhist=0&ajaxserp=0",
                                                    "https://www.bing.com/images/search?view=detailV2&ccid=E8o%2fK1fx&id=229A607F99275ACAFBD23306524F821BABC8EF17&thid=OIP.E8o_K1fxipaLpsW_E204qAHaEo&mediaurl=https%3a%2f%2fth.bing.com%2fth%2fid%2fR.13ca3f2b57f18a968ba6c5bf136d38a8%3frik%3dF%252b%252fIqxuCT1IGMw%26riu%3dhttp%253a%252f%252fi1.hdslb.com%252fbfs%252farchive%252f9ba8c4973464955c50f5bdeaee3f72492148e453.jpg%26ehk%3dL8EzMNQV25RJinfZ6Vb4PMM55YGVbvN7hkyI5AiDBNk%253d%26risl%3d%26pid%3dImgRaw%26r%3d0&exph=1080&expw=1728&q=%e5%8c%bb%e5%ad%a6%e7%9f%a5%e8%af%86%e5%9b%be%e8%b0%b1%e7%9a%84%e5%9b%be%e7%89%87&simid=608017170348927201&FORM=IRPRST&ck=5FC629A2BA4693B52489100783BA0072&selectedIndex=18&ajaxhist=0&ajaxserp=0".
        "https://www.bing.com/images/search?view=detailV2&ccid=ltpekDce&id=44C97C1B9B40FE21B28983E49E5421462A694F39&thid=OIP.ltpekDceztbP7FcWRMKQEQHaEK&mediaurl=https%3a%2f%2fpic4.zhimg.com%2fv2-aa9a3ce2af7dda8277cd13e354a3d69b_r.jpg&exph=608&expw=1080&q=%e5%8c%bb%e5%ad%a6%e7%9f%a5%e8%af%86%e5%9b%be%e8%b0%b1%e7%9a%84%e5%9b%be%e7%89%87&simid=608003027006613477&FORM=IRPRST&ck=81F85C9CD57E3CB0BC983938B177ED1D&selectedIndex=53&ajaxhist=0&ajaxserp=0",
                                                    "https://www.bing.com/images/search?view=detailV2&ccid=EZm4e%2fgf&id=F82B9589DE73AE329DB0348347D15C39A2038850&thid=OIP.EZm4e_gfENDIKcgfJ1dEwgHaF1&mediaurl=https%3a%2f%2fth.bing.com%2fth%2fid%2fR.1199b87bf81f10d0c829c81f275744c2%3frik%3dUIgDojlc0UeDNA%26riu%3dhttp%253a%252f%252fimage.sciencenet.cn%252falbum%252f201408%252f14%252f035538ig75f9c25ckufkq7.png%26ehk%3dqvGvBpGXVY8H42BUSVnmx11XgqvrXkbbTqlre97d7C0%253d%26risl%3d%26pid%3dImgRaw%26r%3d0&exph=500&expw=635&q=%e5%8c%bb%e5%ad%a6%e7%9f%a5%e8%af%86%e5%9b%be%e8%b0%b1%e7%9a%84%e5%9b%be%e7%89%87&simid=608044950207596755&FORM=IRPRST&ck=9A5A6F2D649CC00C7932BBC56FF570DC&selectedIndex=28&ajaxhist=0&ajaxserp=0",
                                                    "https://www.bing.com/images/search?view=detailV2&ccid=EZm4e%2fgf&id=F82B9589DE73AE329DB0348347D15C39A2038850&thid=OIP.EZm4e_gfENDIKcgfJ1dEwgHaF1&mediaurl=https%3a%2f%2fth.bing.com%2fth%2fid%2fR.1199b87bf81f10d0c829c81f275744c2%3frik%3dUIgDojlc0UeDNA%26riu%3dhttp%253a%252f%252fimage.sciencenet.cn%252falbum%252f201408%252f14%252f035538ig75f9c25ckufkq7.png%26ehk%3dqvGvBpGXVY8H42BUSVnmx11XgqvrXkbbTqlre97d7C0%253d%26risl%3d%26pid%3dImgRaw%26r%3d0&exph=500&expw=635&q=%e5%8c%bb%e5%ad%a6%e7%9f%a5%e8%af%86%e5%9b%be%e8%b0%b1%e7%9a%84%e5%9b%be%e7%89%87&simid=608044950207596755&FORM=IRPRST&ck=9A5A6F2D649CC00C7932BBC56FF570DC&selectedIndex=28&ajaxhist=0&ajaxserp=0"
                                                ],
                                                    //目前下面这4个值不用
                                                    "journal": "期刊1",
                                                        "citationCount": 50,
                                                            "favoriteCount": 20,
                                                                "noteCount": 5
}



//参考文献
{
    "referenceCount": 27,
        "papers": [
            "S.Abeywickrama,R.Zhang, Q.Wu,and C.Yuen,Intelligentd re- flecting surface: Practical phase shift model and beamforming opti- mization,Submitted to IEEE Trans. Commun., [Online]. Available: https://arxiv.org/abs/2002.10112.",
            "Q. Wu and R. Zhang, Intelligent reflecting surface enhanced wireless network via joint active and passive beamforming IEEE Trans. Wireless Commun., vol. 18, no. 11, pp. 5394-5409, Nov. 2019.",
            "Q. Wu and R. Zhang, Towards smart and reconfigurable environment: Intelligent reflecting surface aided wireless network,IEEE Commun. Mag., vol. 58, no. 1, pp. 106-112, Jan. 2020.",
            "C. Huang, A. Zappone, G. C. Alexandropoulos, M. Debbah, and C. Yuen, Reconfigurable intelligent surfaces for energy efficiency in wireless communication,IEEE Trans. Wireless Commun., vol. 18, no. 8, pp. 4157-4170, Aug. 2019.",
            "E. Basar, M. Di Renzo, J. De Rosny, M. Debbah, M. Alouini, and R. Zhang, Wireless communications through reconfigurable intelligent surfaces,IEEE Access, vol. 7, pp. 116 753-116 773, Aug. 2019.",
            "H. Rajagopalan and Y. Rahmat-Samii, Loss quantification for mi- crostrip reflectarray: Issue of high fields and currents,in Proc. IEEE Antennas and Propag. Society Int. Symposium, Jul. 2008, pp. 1-4.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
            "B. Zheng and R. Zhang, Intelligent reflecting surface-enhanced OFDM: Channel estimation and reflection optimization,IEEE Wireless Com- mun. Lett., DOI:10.1109/LWC.2019.2961357, Dec. 2019.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
            "Y. Yang, B. Zheng, S. Zhang, and R. Zhang, Intelligent reflecting surface meets OFDM: Protocol design and rate maximization,[Online].""S.Abeywickrama,R.Zhang, Q.Wu,and C.Yuen,Intelligentd re- flecting surface: Practical phase shift model and beamforming opti- mization,Submitted to IEEE Trans. Commun., [Online]. Available: https://arxiv.org/abs/2002.10112.",
            "Q. Wu and R. Zhang, Intelligent reflecting surface enhanced wireless network via joint active and passive beamforming IEEE Trans. Wireless Commun., vol. 18, no. 11, pp. 5394-5409, Nov. 2019.",
            "Q. Wu and R. Zhang, Towards smart and reconfigurable environment: Intelligent reflecting surface aided wireless network,IEEE Commun. Mag., vol. 58, no. 1, pp. 106-112, Jan. 2020.",
            "C. Huang, A. Zappone, G. C. Alexandropoulos, M. Debbah, and C. Yuen, Reconfigurable intelligent surfaces for energy efficiency in wireless communication,IEEE Trans. Wireless Commun., vol. 18, no. 8, pp. 4157-4170, Aug. 2019.",
            "E. Basar, M. Di Renzo, J. De Rosny, M. Debbah, M. Alouini, and R. Zhang, Wireless communications through reconfigurable intelligent surfaces,IEEE Access, vol. 7, pp. 116 753-116 773, Aug. 2019.",
            "H. Rajagopalan and Y. Rahmat-Samii, Loss quantification for mi- crostrip reflectarray: Issue of high fields and currents,in Proc. IEEE Antennas and Propag. Society Int. Symposium, Jul. 2008, pp. 1-4.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
            "B. Zheng and R. Zhang, Intelligent reflecting surface-enhanced OFDM: Channel estimation and reflection optimization,IEEE Wireless Com- mun. Lett., DOI:10.1109/LWC.2019.2961357, Dec. 2019.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
            "Y. Yang, B. Zheng, S. Zhang, and R. Zhang, Intelligent reflecting surface meets OFDM: Protocol design and rate maximization,[Online].",
            "S.Abeywickrama,R.Zhang, Q.Wu,and C.Yuen,Intelligentd re- flecting surface: Practical phase shift model and beamforming opti- mization,Submitted to IEEE Trans. Commun., [Online]. Available: https://arxiv.org/abs/2002.10112.",
            "Q. Wu and R. Zhang, Intelligent reflecting surface enhanced wireless network via joint active and passive beamforming IEEE Trans. Wireless Commun., vol. 18, no. 11, pp. 5394-5409, Nov. 2019.",
            "Q. Wu and R. Zhang, Towards smart and reconfigurable environment: Intelligent reflecting surface aided wireless network,IEEE Commun. Mag., vol. 58, no. 1, pp. 106-112, Jan. 2020.",
            "C. Huang, A. Zappone, G. C. Alexandropoulos, M. Debbah, and C. Yuen, Reconfigurable intelligent surfaces for energy efficiency in wireless communication,IEEE Trans. Wireless Commun., vol. 18, no. 8, pp. 4157-4170, Aug. 2019.",
            "E. Basar, M. Di Renzo, J. De Rosny, M. Debbah, M. Alouini, and R. Zhang, Wireless communications through reconfigurable intelligent surfaces,IEEE Access, vol. 7, pp. 116 753-116 773, Aug. 2019.",
            "H. Rajagopalan and Y. Rahmat-Samii, Loss quantification for mi- crostrip reflectarray: Issue of high fields and currents,in Proc. IEEE Antennas and Propag. Society Int. Symposium, Jul. 2008, pp. 1-4.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
            "B. Zheng and R. Zhang, Intelligent reflecting surface-enhanced OFDM: Channel estimation and reflection optimization,IEEE Wireless Com- mun. Lett., DOI:10.1109/LWC.2019.2961357, Dec. 2019.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
        ],
}

//被引用
{
    "referenceCount": 37,
        "papers": [
            "S.Abeywickrama,R.Zhang, Q.Wu,and C.Yuen,Intelligentd re- flecting surface: Practical phase shift model and beamforming opti- mization,Submitted to IEEE Trans. Commun., [Online]. Available: https://arxiv.org/abs/2002.10112.",
            "Q. Wu and R. Zhang, Intelligent reflecting surface enhanced wireless network via joint active and passive beamforming IEEE Trans. Wireless Commun., vol. 18, no. 11, pp. 5394-5409, Nov. 2019.",
            "Q. Wu and R. Zhang, Towards smart and reconfigurable environment: Intelligent reflecting surface aided wireless network,IEEE Commun. Mag., vol. 58, no. 1, pp. 106-112, Jan. 2020.",
            "C. Huang, A. Zappone, G. C. Alexandropoulos, M. Debbah, and C. Yuen, Reconfigurable intelligent surfaces for energy efficiency in wireless communication,IEEE Trans. Wireless Commun., vol. 18, no. 8, pp. 4157-4170, Aug. 2019.",
            "E. Basar, M. Di Renzo, J. De Rosny, M. Debbah, M. Alouini, and R. Zhang, Wireless communications through reconfigurable intelligent surfaces,IEEE Access, vol. 7, pp. 116 753-116 773, Aug. 2019.",
            "H. Rajagopalan and Y. Rahmat-Samii, Loss quantification for mi- crostrip reflectarray: Issue of high fields and currents,in Proc. IEEE Antennas and Propag. Society Int. Symposium, Jul. 2008, pp. 1-4.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
            "B. Zheng and R. Zhang, Intelligent reflecting surface-enhanced OFDM: Channel estimation and reflection optimization,IEEE Wireless Com- mun. Lett., DOI:10.1109/LWC.2019.2961357, Dec. 2019.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
            "Y. Yang, B. Zheng, S. Zhang, and R. Zhang, Intelligent reflecting surface meets OFDM: Protocol design and rate maximization,[Online].""S.Abeywickrama,R.Zhang, Q.Wu,and C.Yuen,Intelligentd re- flecting surface: Practical phase shift model and beamforming opti- mization,Submitted to IEEE Trans. Commun., [Online]. Available: https://arxiv.org/abs/2002.10112.",
            "Q. Wu and R. Zhang, Intelligent reflecting surface enhanced wireless network via joint active and passive beamforming IEEE Trans. Wireless Commun., vol. 18, no. 11, pp. 5394-5409, Nov. 2019.",
            "Q. Wu and R. Zhang, Towards smart and reconfigurable environment: Intelligent reflecting surface aided wireless network,IEEE Commun. Mag., vol. 58, no. 1, pp. 106-112, Jan. 2020.",
            "C. Huang, A. Zappone, G. C. Alexandropoulos, M. Debbah, and C. Yuen, Reconfigurable intelligent surfaces for energy efficiency in wireless communication,IEEE Trans. Wireless Commun., vol. 18, no. 8, pp. 4157-4170, Aug. 2019.",
            "E. Basar, M. Di Renzo, J. De Rosny, M. Debbah, M. Alouini, and R. Zhang, Wireless communications through reconfigurable intelligent surfaces,IEEE Access, vol. 7, pp. 116 753-116 773, Aug. 2019.",
            "H. Rajagopalan and Y. Rahmat-Samii, Loss quantification for mi- crostrip reflectarray: Issue of high fields and currents,in Proc. IEEE Antennas and Propag. Society Int. Symposium, Jul. 2008, pp. 1-4.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
            "B. Zheng and R. Zhang, Intelligent reflecting surface-enhanced OFDM: Channel estimation and reflection optimization,IEEE Wireless Com- mun. Lett., DOI:10.1109/LWC.2019.2961357, Dec. 2019.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
            "Y. Yang, B. Zheng, S. Zhang, and R. Zhang, Intelligent reflecting surface meets OFDM: Protocol design and rate maximization,[Online].",
            "S.Abeywickrama,R.Zhang, Q.Wu,and C.Yuen,Intelligentd re- flecting surface: Practical phase shift model and beamforming opti- mization,Submitted to IEEE Trans. Commun., [Online]. Available: https://arxiv.org/abs/2002.10112.",
            "Q. Wu and R. Zhang, Intelligent reflecting surface enhanced wireless network via joint active and passive beamforming IEEE Trans. Wireless Commun., vol. 18, no. 11, pp. 5394-5409, Nov. 2019.",
            "Q. Wu and R. Zhang, Towards smart and reconfigurable environment: Intelligent reflecting surface aided wireless network,IEEE Commun. Mag., vol. 58, no. 1, pp. 106-112, Jan. 2020.",
            "C. Huang, A. Zappone, G. C. Alexandropoulos, M. Debbah, and C. Yuen, Reconfigurable intelligent surfaces for energy efficiency in wireless communication,IEEE Trans. Wireless Commun., vol. 18, no. 8, pp. 4157-4170, Aug. 2019.",
            "E. Basar, M. Di Renzo, J. De Rosny, M. Debbah, M. Alouini, and R. Zhang, Wireless communications through reconfigurable intelligent surfaces,IEEE Access, vol. 7, pp. 116 753-116 773, Aug. 2019.",
            "H. Rajagopalan and Y. Rahmat-Samii, Loss quantification for mi- crostrip reflectarray: Issue of high fields and currents,in Proc. IEEE Antennas and Propag. Society Int. Symposium, Jul. 2008, pp. 1-4.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
            "B. Zheng and R. Zhang, Intelligent reflecting surface-enhanced OFDM: Channel estimation and reflection optimization,IEEE Wireless Com- mun. Lett., DOI:10.1109/LWC.2019.2961357, Dec. 2019.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
            "Y. Yang, B. Zheng, S. Zhang, and R. Zhang, Intelligent reflecting surface meets OFDM: Protocol design and rate maximization,[Online].""S.Abeywickrama,R.Zhang, Q.Wu,and C.Yuen,Intelligentd re- flecting surface: Practical phase shift model and beamforming opti- mization,Submitted to IEEE Trans. Commun., [Online]. Available: https://arxiv.org/abs/2002.10112.",
            "Q. Wu and R. Zhang, Intelligent reflecting surface enhanced wireless network via joint active and passive beamforming IEEE Trans. Wireless Commun., vol. 18, no. 11, pp. 5394-5409, Nov. 2019.",
            "Q. Wu and R. Zhang, Towards smart and reconfigurable environment: Intelligent reflecting surface aided wireless network,IEEE Commun. Mag., vol. 58, no. 1, pp. 106-112, Jan. 2020.",
            "C. Huang, A. Zappone, G. C. Alexandropoulos, M. Debbah, and C. Yuen, Reconfigurable intelligent surfaces for energy efficiency in wireless communication,IEEE Trans. Wireless Commun., vol. 18, no. 8, pp. 4157-4170, Aug. 2019.",
            "E. Basar, M. Di Renzo, J. De Rosny, M. Debbah, M. Alouini, and R. Zhang, Wireless communications through reconfigurable intelligent surfaces,IEEE Access, vol. 7, pp. 116 753-116 773, Aug. 2019.",
            "H. Rajagopalan and Y. Rahmat-Samii, Loss quantification for mi- crostrip reflectarray: Issue of high fields and currents,in Proc. IEEE Antennas and Propag. Society Int. Symposium, Jul. 2008, pp. 1-4.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
            "B. Zheng and R. Zhang, Intelligent reflecting surface-enhanced OFDM: Channel estimation and reflection optimization,IEEE Wireless Com- mun. Lett., DOI:10.1109/LWC.2019.2961357, Dec. 2019.",
            "Y. Zeng, B. Clerckx, and R. Zhang, Communications and signals design for wireless power transmission,IEEE Trans. Commun., vol. 65, no. 5, pp. 2264-2290, May 2017.",
            "Y. Yang, B. Zheng, S. Zhang, and R. Zhang, Intelligent reflecting surface meets OFDM: Protocol design and rate maximization,[Online]."
        ],
}